#!/bin/bash

ids=${TELEGRAM_CHAT_ID//,/$'\n'}
for id in $ids
do  
    MESSAGE=""
    if [ "$CI_JOB_STATUS" = "success" ]; then
        MESSAGE="$1 $CI_JOB_NAME job is succed 🚀"
    elif [ "$CI_JOB_STATUS" = "failed" ]; then
        MESSAGE="$1 $CI_JOB_NAME job is failed 🆘"
    else
        MESSAGE="$1 $CI_JOB_NAME job is cancelled 🔙"
    fi
    TIME="10"
    URL="https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage"
    TEXT="CI/CD status: $MESSAGE%0A%0AProject:+$CI_PROJECT_NAME%0AURL:+$CI_PROJECT_URL/pipelines/$CI_PIPELINE_ID/%0ABranch:+$CI_COMMIT_REF_SLUG%0AUser:+$GITLAB_USER_NAME"
    curl -s --max-time $TIME -d "chat_id=$id&disable_web_page_preview=1&text=$TEXT" $URL > /dev/null
done