FROM node:16-alpine3.18 AS builder

WORKDIR /app

COPY ./package.json /app/package.json
COPY ./package-lock.json /app/package-lock.json

RUN npm install
COPY . .
RUN npm run build

FROM nginx:stable-alpine3.17

COPY --from=builder /app/build /usr/share/nginx/html
